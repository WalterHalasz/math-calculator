Math-Calculator
===============

The best practice of programming can be done by mathematical functions and algorithms, so once every programmer makes or must make a calculator , which display the basic understanding and structure of programme, it can be a simple one or a complex structure, all based on the level, preference and the desired outcome. Find more about them [here](http://www.pay-dayloans.com.au/).




-- The structure for the project 

  - The user first must create an external text file and fill it with math problems that they want solved. 
    Don't worry about the structure of the document, all text and non math symbols will be ignored by this program.
    
  - After the user has created the text file, open the program on command line and input the math text file that you
    want solved. The program will then crawl through the text information until a math equation is found. Then the program
    parses the information into variables that then are used to perform mathematical equations on.
  
  - Finally, the information after computer, is sent to a newely created text file outside of the program, but within the project folder.